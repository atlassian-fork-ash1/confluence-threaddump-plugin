package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;

/**
 * Generates a thread dump and renders it to the web UI
 */
public class GenerateThreadDumpAction extends ConfluenceActionSupport
{
    private String threadDumpOutput;
    private ThreadDumpBuilder threadDumpBuilder;

    public String execute() throws Exception
    {
        threadDumpOutput = threadDumpBuilder.build();
        return super.execute();
    }

    public void setThreadDumpBuilder(ThreadDumpBuilder threadDumpBuilder)
    {
        this.threadDumpBuilder = threadDumpBuilder;
    }

    public String getThreadDumpOutput()
    {
        return threadDumpOutput;
    }

    @Override
    public boolean isPermitted()
    {
        return permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM);
    }
}
