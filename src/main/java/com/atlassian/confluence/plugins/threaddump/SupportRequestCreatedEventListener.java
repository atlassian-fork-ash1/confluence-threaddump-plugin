package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.confluence.event.events.support.SupportRequestCreatedEvent;
import com.atlassian.event.api.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Register itself as a listener to SupportRequestCreatedEvent (part of Confluence-3.3) and will write a thread dump to
 * the warn log for this class when the event is fired.
 */
public class SupportRequestCreatedEventListener
{

    private ThreadDumpBuilder threadDumpBuilder;

    public void setThreadDumpBuilder(ThreadDumpBuilder threadDumpBuilder)
    {
        this.threadDumpBuilder = threadDumpBuilder;
    }

    /**
     * Called when the SupportRequestCreatedEvent is fired. Will generare a thread dump to the warn log.
     * @param event the actual event is ignored.
     */
    @com.atlassian.event.api.EventListener
    public void generateThreadDump(SupportRequestCreatedEvent event)
    {

        Logger log = LoggerFactory.getLogger(ThreadDumpBuilder.class);
        log.warn(threadDumpBuilder.build());
        
    }

    /**
     * Called by spring when the component is loaded. This method will register the instance as a listener to
     * SupportRequestCreatedEvent.
     * @param eventPublisher publisher to register the listener with.
     */
    public void setEventPublisher(EventPublisher eventPublisher)
    {
        eventPublisher.register(this);
    }
}
